import React from "react";
import { Link } from "react-router-dom";
import HeroBanner from "../components/HeroBanner";
import { ProductConsumer } from "../context";
import singleProductBcg from "../images/singleProductBcg.jpeg";

export default function SingleProductPage() {
  return (
    <>
      <HeroBanner img={singleProductBcg} title="Single Product" />
      <ProductConsumer>
        {(value) => {
          console.log(value);
          const { singleProduct, addToCart, loading } = value;
          if (loading) {
            console.log("single product");
            return <h1>Single Product</h1>;
          }
          const {
            id,
            price,
            image,
            title,
            description,
            company,
          } = singleProduct;

          return (
            <section className="py-5">
              <div className="container">
                <div className="row">
                  <div className="col-10 mx-auto col-md-6 col-sm-8 my-3">
                    <img
                      src={`../${image}`}
                      alt={title}
                      className="img-fuild"
                    />
                  </div>
                  <div className="col-10 mx-auto col-md-6 col-sm-8 my-3">
                    <h5 className="text-title mb-4">Model: {title}</h5>
                    <h5 className="text-capitalize text-muted mb-4">
                      Company: {company}
                    </h5>
                    <h5 className="text-main text-capitalize mb-4">
                      Price: {price} €
                    </h5>
                    <p className="text-capitalize text-title">
                      Product Description
                    </p>
                    <p className="text-justify">{description}</p>
                    <button
                      type="button"
                      style={{ margin: "0.75rem" }}
                      className="main-link"
                      onClick={() => addToCart(id)}
                    >
                      Add To Cart
                    </button>
                    <Link
                      to="/products"
                      style={{ margin: "0.75rem" }}
                      className="main-link"
                    >
                      Back To Products
                    </Link>
                  </div>
                </div>
              </div>
            </section>
          );
        }}
      </ProductConsumer>
    </>
  );
}
