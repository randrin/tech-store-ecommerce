import React from "react";
import HeroBanner from "../components/HeroBanner";
import Products from "../components/ProductsPage/Products";
import productsBcg from "../images/productsBcg.jpeg";

export default function ProductsPage() {
  return (
    <>
      <HeroBanner img={productsBcg} />
      <Products />
    </>
  );
}
