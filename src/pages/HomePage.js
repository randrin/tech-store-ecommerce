import React from "react";
import { Link } from "react-router-dom";
import Services from "../components/HomePage/Services";
import FeaturedProduct from "../components/HomePage/FeaturedProduct";
import HeroBanner from "../components/HeroBanner";

export default function HomePage() {
  return (
    <>
      <HeroBanner title="Our Awesome Products" max="true">
        <Link to="/products" className="main-link" style={{ margin: "2rem" }}>
          Visit Products
        </Link>
      </HeroBanner>
      <Services />
      <FeaturedProduct />
    </>
  );
}
