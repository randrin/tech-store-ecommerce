import React from "react";
import { Link } from "react-router-dom";
import HeroBanner from "../components/HeroBanner";
import defaultBcg from '../images/defaultBcg.jpeg';

export default function Default() {
  return (
    <>
      <HeroBanner title="Error 404" max="true" img={defaultBcg}>
        <h2 className="text-uppercase">Page Not Available</h2>
        <Link to="/" className="main-link">Return to Home Page</Link>
      </HeroBanner>
    </>
  );
}
