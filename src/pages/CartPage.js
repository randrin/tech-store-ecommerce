import React from "react";
import HeroBanner from "../components/HeroBanner";
import Cart from "../components/CartPage";
import storeBcg from "../images/storeBcg.jpeg";

export default function CartPage(props) {
  return (
    <>
      <HeroBanner img={storeBcg} />
      <Cart history={props.history} />
    </>
  );
}
