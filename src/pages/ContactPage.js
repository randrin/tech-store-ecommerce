import React from "react";
import HeroBanner from "../components/HeroBanner";
import ContactBcg from "../images/contactBcg.jpeg";
import Contact from "../components/ContactPage/Contact";

export default function ContactPage() {
  return (
    <>
      <HeroBanner img={ContactBcg} title="Contact Us" />
      <Contact />
    </>
  );
}
