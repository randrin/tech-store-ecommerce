import React from "react";
import Info from "../components/AboutPage/Info";
import HeroBanner from '../components/HeroBanner';
import aboutBcg from '../images/aboutBcg.jpeg';

export default function AboutPage() {
  return (
    <>
      <HeroBanner img={aboutBcg} />
      <Info />
    </>
  );
}
