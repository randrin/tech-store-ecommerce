import React from "react";
import styled from "styled-components";

export default function Title({ title, center }) {
  return (
    <TitleWrapper className="row" center={center}>
      <div className="col">
        <div className="text-title">{title}</div>
        <div className="title-underline"></div>
      </div>
    </TitleWrapper>
  );
}

const TitleWrapper = styled.div`
  text-align: ${(props) => (props.center ? "center" : "left")};
  .text-title {
    font-size: 2.5rem;
    text-shadow: 4px 4px 2px rgba(150, 150, 150, 1);
    text-transform: uppercase;
  }
  .title-underline {
    height: 0.25rem;
    width: 7rem;
    background: var(--primaryColor);
    margin: ${(props) => (props.center ? "0 auto" : "0")};
  }
`;
