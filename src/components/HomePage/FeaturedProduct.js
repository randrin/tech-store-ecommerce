import React from "react";
import { Link } from "react-router-dom";
import Product from "../../components/Product";
import Title from "../Title";
import { ProductConsumer } from "../../context";

export default function FeaturedProduct() {
  return (
    <div className="container py-5">
      {/* Title */}
      <Title title="Featured Products" center="true" />
      {/* Products */}
      <div className="row mt-5">
        <ProductConsumer>
          {(value) => {
            const { featuredProducts } = value;
            return featuredProducts.map((product) => (
              <Product key={product.id} product={product} />
            ));
          }}
        </ProductConsumer>
      </div>
      <div className="row mt-5">
        <div className="col text-center">
          <Link to="/products" className="main-link">
            Go to All Products
          </Link>
        </div>
      </div>
    </div>
  );
}
