import React from "react";
import styled from "styled-components";
import { ProductConsumer } from "../../context";

export default function Services() {
  return (
    <ProductConsumer>
      {(value) => {
        return (
          <ServicesWrapper>
            <div className="container py-5">
              <div className="row">
                {value.services.map((service) => (
                  <div
                    key={service.id}
                    className="col-md-4 col-sm-6 col-10 mx-auto text-center"
                  >
                    <div>{service.icon}</div>
                    <div className="mt-3 service-title text-capitalize">
                      {service.title}
                    </div>
                    <div className="mt-3">{service.text}</div>
                  </div>
                ))}
              </div>
            </div>
          </ServicesWrapper>
        );
      }}
    </ProductConsumer>
  );
}

const ServicesWrapper = styled.section`
  background: var(--primaryColor);
  color: var(--mainWhite);
  .service-title {
    font-size: 2rem;
    color: var(--mainBlack);
  }
  .icon {
    color: var(--primaryRGBA);
    font-size: 2rem;
  }
`;
