import React from "react";
import Title from "../../components/Title";

export default function Contact() {
  return (
    <section className="py-5">
      <div className="container">
        <div className="row">
          <div className="col-10 mx-auto col-md-6 my-3">
            <Title title="Contact Us" />
            <form
              action="https://formspree.io/nzeukangrandrin@gmail.com"
              method="POST"
              className="mt-5"
            >
              <div className="form-group">
                <input
                  type="text"
                  className="form-control"
                  name="firstName"
                  placeholder="Enter your First Name"
                />
              </div>
              <div className="form-group">
                <input
                  type="email"
                  className="form-control"
                  name="email"
                  placeholder="Enter your Email"
                />
              </div>
              <div className="form-group">
                <input
                  type="text"
                  className="form-control"
                  name="subject"
                  placeholder="Enter your Subject"
                />
              </div>
              <div className="form-group">
                <textarea
                  className="form-control"
                  name="message"
                  rows="10"
                  placeholder="Enter your Message"
                ></textarea>
              </div>
              <div className="form-group m-2">
                <button type="submit" className="main-link">
                  Send Message
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  );
}
