import React from "react";
import styled from "styled-components";
import { ProductConsumer } from "../context";

export default function Footer() {
  return (
    <ProductConsumer>
      {(value) => {
        return (
          <FooterWrapper>
            <div className="container py-3">
              <div className="row">
                <div className="col-md-6">
                  <p className="text-capitalize">
                    Copyrigth &copy; {new Date().getFullYear()}. All right
                    reserved.
                  </p>
                </div>
                <div className="col-md-6">
                  {value.socialIcons.map((item) => (
                    <a
                      key={item.id}
                      href={item.url}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      {item.icon}
                    </a>
                  ))}
                </div>
              </div>
            </div>
          </FooterWrapper>
        );
      }}
    </ProductConsumer>
  );
}

const FooterWrapper = styled.footer`
  background: var(--darkGrey);
  color: var(--mainWhite);
  .icon {
    font-size: 1.5rem;
    color: var(--mainWhite);
    transition: var(--mainTransition);
    cursor: pointer;
    margin: 0 10px;
  }
  .icon:hover {
    color: var(--primaryColor);
  }
`;
