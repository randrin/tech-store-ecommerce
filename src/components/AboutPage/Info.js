import React from "react";
import { Link } from "react-router-dom";
import Title from "../Title";
import aboutBcg from "../../images/aboutBcg.jpeg";

export default function Info() {
  return (
    <section className="py-5">
      <div className="container">
        <div className="row">
          <div className="col-10 mx-auto col-md-6 my-3">
            <img src={aboutBcg} className="img-fuild img-thumbnail" alt="About Store" />
          </div>
          <div className="col-10 mx-auto col-md-6 my-3">
            <Title title="About Us" />
            <p className="mt-3 text-justify">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged. It was
              popularised in the 1960s with the release of Letraset sheets
              containing Lorem Ipsum passages, and more recently with desktop
              publishing software like Aldus PageMaker including versions of
              Lorem Ipsum.
            </p>
            <Link
              to="/products"
              className="main-link"
              style={{ margin: "2rem 0" }}
            >
              Visit Products
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
}
