import React from "react";
import Title from "../Title";
import { ProductConsumer } from "../../context";
import Product from "../../components/Product";
import FilteredProducts from "./FilterProducts";
import { FaFrown } from "react-icons/fa";

export default function Products() {
  return (
    <ProductConsumer>
      {(value) => {
        const { filteredProducts } = value;
        return (
          <section className="py-5">
            <div className="container">
              <div className="row">
                <div className="col-10 mx-auto col-md-6 my-3">
                  {/* Title */}
                  <Title title="Our Products" center="true" />
                </div>
              </div>
              {/* Filter Products */}
              <div className="row mt-5 justify-content-center">
                <div className="col-10">
                  <FilteredProducts />
                </div>
              </div>
              <div className="row mt-2 justify-content-center">
                <div className="col-10">
                  <h6 className="text-title text-danger">
                    Total Products: {filteredProducts.length}
                  </h6>
                </div>
              </div>
              <hr />
              {/* Products */}
              <div className="row mt-5 justify-content-center">
                {filteredProducts.length === 0 ? (
                  <div className="text-center">
                    <FaFrown
                      className="text-danger m-5"
                      style={{ fontSize: "60px" }}
                    />
                    <h6 className="text-title">
                      No items found, try another one
                    </h6>
                  </div>
                ) : (
                  filteredProducts.map((product) => (
                    <Product key={product.id} product={product} />
                  ))
                )}
              </div>
            </div>
          </section>
        );
      }}
    </ProductConsumer>
  );
}
