import React from "react";
import { ProductConsumer } from "../../context";
import styled from "styled-components";

export default function FiltedProducts() {
  return (
    <ProductConsumer>
      {(value) => {
        const {
          handleChange,
          min,
          max,
          price,
          company,
          search,
          shipping,
          storeProducts,
        } = value;
        let companies = new Set();
        companies.add("all");
        for (let product in storeProducts) {
          companies.add(storeProducts[product]["company"]);
        }
        companies = [...companies];
        return (
          <div className="row my-5">
            <div className="col">
              <FilterWrapper>
                <div className="row">
                  {/** Search Product */}
                  <div className="col-md-3 col-sm-10">
                    <label htmlFor="search" className="font-weight-bold">
                      Search Product
                    </label>
                    <input
                      id="search"
                      type="text"
                      className="form-control"
                      name="search"
                      value={search}
                      onChange={handleChange}
                    />
                  </div>
                  {/** END Search Product */}
                  {/** Select Company */}
                  <div className="col-md-3 col-sm-10">
                    <label htmlFor="company" className="font-weight-bold">
                      Company Product
                    </label>
                    <select
                      id="company"
                      className="form-control"
                      name="company"
                      value={company}
                      onChange={handleChange}
                    >
                      {/* <option value="all">All</option>
                      {/* {storeProducts.map((company, index) => {
                        return (
                          <option key={index} value={company.company}>
                            {company.company}
                          </option>
                        );
                      })} */}{" "}
                      */}
                      {companies.map((company, index) => {
                        return (
                          <option
                            key={index}
                            value={company}
                            className="text-capitalize"
                          >
                            {company}
                          </option>
                        );
                      })}
                    </select>
                  </div>
                  {/** END Select Company */}
                  {/** Select Price */}
                  <div className="col-md-3 col-sm-10">
                    <label htmlFor="price" className="font-weight-bold">
                      <p className="mb-2">
                        Price Product: <span> {price} €</span>
                      </p>
                    </label>
                    <input
                      id="price"
                      type="range"
                      max={max}
                      min={min}
                      className="form-control"
                      name="price"
                      value={price}
                      onChange={handleChange}
                    />
                  </div>
                  {/** END Select Price */}
                  {/** Select Shipping */}
                  <div className="col-md-3 col-sm-10">
                    <label htmlFor="shipping" className="font-weight-bold">
                      Free Shipping ?
                    </label>
                    <input
                      id="shipping"
                      type="checkbox"
                      className="formShipping"
                      name="shipping"
                      checked={shipping}
                      onChange={handleChange}
                    />
                  </div>
                  {/** END Select Shipping */}
                </div>
              </FilterWrapper>
            </div>
          </div>
        );
      }}
    </ProductConsumer>
  );
}

const FilterWrapper = styled.div`
  .formShipping {
    margin: 0px 10px;
  }
`;
