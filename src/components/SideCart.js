import React from "react";
import styled from "styled-components";
import { ProductConsumer } from "../context";
import { Link } from "react-router-dom";

export default function SideCart() {
  return (
    <ProductConsumer>
      {(value) => {
        const { cart, cartOpen, closeCart, cartSubTotal } = value;
        let emptyCart = localStorage.getItem("cart") ? true : false;
        return (
          <CartWrapper show={cartOpen} onClick={closeCart}>
            <ul>
              {cart.map((item) => {
                return (
                  <li key={item.id} className="mb-4 text-center cart-item">
                    <div className="row">
                      <div className="col-10 mx-auto col-md-6">
                        <img
                          style={{ width: "35px" }}
                          src={`../${item.image}`}
                          alt={item.title}
                        />
                      </div>
                      <div className="col-10 mx-auto col-md-6 font-weight-bold text-danger">
                        {item.total} €
                      </div>
                    </div>
                    <div className="mt-3">
                      <h6 className="text-capitalize">{item.title}</h6>
                      <h6 className="text-title text-capitalize">
                        {" "}
                        Number: {item.count}
                      </h6>
                    </div>
                  </li>
                );
              })}
            </ul>
            <div className="text-center">
              <h4 className="text-capitalize text-main">
                Cart Total: {cartSubTotal} €
              </h4>
              {emptyCart ? (
                <Link to="/cart" className="main-link my-4">
                  Go To cart
                </Link>
              ) : (
                <Link to="/products" className="main-link my-4">
                  Go To Products
                </Link>
              )}
            </div>
          </CartWrapper>
        );
      }}
    </ProductConsumer>
  );
}

const CartWrapper = styled.div`
  position: fixed;
  top: 60px;
  right: 0;
  width: 100%;
  height: 100%;
  background: var(--mainGrey);
  z-index: 1;
  border-left: 4px solid var(--primaryColor);
  transition: var(--mainTransition);
  transform: ${(props) => (props.show ? "translateX(0)" : "translateX(100%)")};
  @media (min-width: 576px) {
    width: 20rem;
  }
  overflow: scroll;
  padding: 2rem;
  ul {
    padding: 0 !important;
  }
  .cart-item {
    list-style: none;
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
  }
`;
