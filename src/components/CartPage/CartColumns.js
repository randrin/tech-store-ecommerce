import React from "react";
import { ProductConsumer } from "../../context";

export default function CartColumns() {
  return (
    <ProductConsumer>
      {(value) => {
        const { cart } = value;
        if (cart.length === 0) {
          return null;
        } else {
          return (
            <div className="container-fluid text-center d-none d-lg-block my-5">
              <div className="row text-title">
                <div className="col-lg-2">
                  <p className="text-uppercase">Image</p>
                </div>
                <div className="col-lg-2">
                  <p className="text-uppercase">Product Name</p>
                </div>
                <div className="col-lg-2">
                  <p className="text-uppercase">Price</p>
                </div>
                <div className="col-lg-2">
                  <p className="text-uppercase">Quantity</p>
                </div>
                <div className="col-lg-2">
                  <p className="text-uppercase">Remove</p>
                </div>
                <div className="col-lg-2">
                  <p className="text-uppercase">Total</p>
                </div>
              </div>
              <hr/>
            </div>
          );
        }
      }}
    </ProductConsumer>
  );
}
