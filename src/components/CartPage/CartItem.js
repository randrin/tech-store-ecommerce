import React from "react";
import styled from "styled-components";
import {
  FaTrashAlt,
  FaChevronCircleUp,
  FaChevronCircleDown,
} from "react-icons/fa";

export default function CartItem({
  cartItem,
  incrementItemCart,
  decrementItemCart,
  removeItemCart,
}) {
  return (
    <CartItemWrapper className="container-fluid text-center d-lg-block">
      <div className="row mt-5 mt-lg-0 text-capitalize text-center align-items-center">
        <div className="col-lg-2 col-10 mx-auto pb-2">
          <img
            className="img-fluid"
            src={cartItem.image}
            alt={cartItem.title}
            style={{ width: "60px" }}
          />
        </div>
        <div className="col-lg-2 col-10 mx-auto pb-2">
          <p className="text-capitalize font-weight-bold">{cartItem.title}</p>
        </div>
        <div className="col-lg-2 col-10 mx-auto pb-2">
          <p className="text-uppercase font-weight-bold text-danger">
            {cartItem.price} €
          </p>
        </div>
        <div className="col-lg-2 col-10 mx-auto pb-2">
          <p className="text-uppercase">
            <FaChevronCircleDown
              onClick={() => decrementItemCart(cartItem.id)}
              className="text-primary"
              style={{ margin: "0px 10px", cursor: "pointer" }}
            />
            <span className="text-title">{cartItem.count}</span>
            <FaChevronCircleUp
              onClick={() => incrementItemCart(cartItem.id)}
              className="text-primary"
              style={{ margin: "0px 10px", cursor: "pointer" }}
            />
          </p>
        </div>
        <div className="col-lg-2 col-10 mx-auto pb-2">
          <p className="text-uppercase">
            <FaTrashAlt
              className="text-danger"
              style={{ cursor: "pointer" }}
              onClick={() => removeItemCart(cartItem.id)}
            />
          </p>
        </div>
        <div className="col-lg-2 col-10 mx-auto pb-2">
          <p className="text-uppercase font-weight-bold text-success">
            {cartItem.total} €
          </p>
        </div>
      </div>
    </CartItemWrapper>
  );
}

const CartItemWrapper = styled.div`
  .row {
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
    margin: 25px 0px;
  }
`;
