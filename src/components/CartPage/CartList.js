import React from "react";
import CartItem from "./CartItem";
import { Link } from "react-router-dom";
import { ProductConsumer } from "../../context";
import { FaShoppingCart } from "react-icons/fa";

export default function CartList() {
  return (
    <div className="container-fluid py-5 my-5">
      <div className="row">
        <div className="col">
          <ProductConsumer>
            {(value) => {
              const {
                cart,
                incrementItemCart,
                decrementItemCart,
                removeItemCart,
              } = value;
              if (cart.length === 0) {
                return (
                  <div className="text-center">
                    <FaShoppingCart
                      className="text-danger m-5"
                      style={{ fontSize: "60px" }}
                    />
                    <h6 className="text-title">Your shopping cart is empty</h6>
                    <Link to="/products" className="main-link mt-4">
                      Go to Products
                    </Link>
                  </div>
                );
              }
              return (
                <>
                  {cart.map((item) => (
                    <CartItem
                      key={item.id}
                      cartItem={item}
                      incrementItemCart={incrementItemCart}
                      decrementItemCart={decrementItemCart}
                      removeItemCart={removeItemCart}
                    />
                  ))}
                </>
              );
            }}
          </ProductConsumer>
        </div>
      </div>
    </div>
  );
}
