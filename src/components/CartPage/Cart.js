import React from "react";
import Title from "../../components/Title";
import CartColumns from "./CartColumns";
import CartTotals from "./CartTotals";
import CartList from "./CartList";

export default function Cart({ history }) {
  return (
    <section className="py-5">
      <div className="container">
        {/** Title */}
        <Title title="Your Shopping Cart" center="true" />
      </div>
      {/** Columns Cart */}
      <CartColumns />
      {/** Items Cart */}
      <CartList />
      {/** Total Cart */}
      <CartTotals history={history} />
    </section>
  );
}
