import React from "react";
import { ProductConsumer } from "../../context";
import PaypalCheckout from "./PaypalCheckout";

export default function CartTotals({ history }) {
  return (
    <div className="container">
      <div className="row">
        <ProductConsumer>
          {(value) => {
            const {
              clearAllItemsCart,
              cartSubTotal,
              cartTax,
              cart,
              cartTotal,
            } = value;
            if (cart.length === 0) {
              return null;
            } else {
              return (
                <div className="col text-title text-center my-4">
                  <button
                    className="btn btn-outline-danger text-capitalize mb-4"
                    onClick={clearAllItemsCart}
                  >
                    Clear Cart
                  </button>
                  <h3>Sub Total: {cartSubTotal} €</h3>
                  <h3>Tax: {cartTax} €</h3>
                  <h3>Total: {cartTotal} €</h3>
                  <PaypalCheckout
                    cartTotal={cartTotal}
                    clearAllItemsCart={clearAllItemsCart}
                    history={history}
                  />
                </div>
              );
            }
          }}
        </ProductConsumer>
      </div>
    </div>
  );
}
