import React from "react";
import {
  FaStoreAlt,
  FaUsers,
  FaGifts,
  FaPhoneSquareAlt,
  FaCartPlus,
} from "react-icons/fa";

export const linkData = [
  {
    id: 1,
    text: "home",
    path: "/",
    icon: <FaStoreAlt className="icon" />,
  },
  {
    id: 2,
    text: "about",
    path: "/about",
    icon: <FaUsers className="icon" />,
  },
  {
    id: 3,
    text: "products",
    path: "/products",
    icon: <FaGifts className="icon" />,
  },
  {
    id: 4,
    text: "contact",
    path: "/contact",
    icon: <FaPhoneSquareAlt className="icon" />,
  },
  {
    id: 5,
    text: "cart",
    path: "/cart",
    icon: <FaCartPlus className="icon" />,
  },
];
