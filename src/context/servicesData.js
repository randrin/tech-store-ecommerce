import React from "react";
import { FaDolly, FaRedo, FaDollarSign } from "react-icons/fa";

export const servicesData = [
  {
    id: 1,
    icon: <FaDolly className="icon" />,
    title: "Free Shipping",
    text:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
  },
  {
    id: 2,
    icon: <FaRedo className="icon" />,
    title: "30 days return policy",
    text:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
  },
  {
    id: 3,
    icon: <FaDollarSign className="icon" />,
    title: "Secured Payments",
    text:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
  },
];
