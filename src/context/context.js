import React, { Component } from "react";
import { linkData } from "./linkData";
import { socialData } from "./socialData";
import { servicesData } from "./servicesData";
import { items } from "./productData";
import { client } from "./contentful";

const ProductContext = React.createContext();

class ProductProvider extends Component {
  state = {
    sidebarOpen: false,
    cartOpen: false,
    cartItems: 0,
    links: linkData,
    socialIcons: socialData,
    cart: [],
    services: servicesData,
    cartSubTotal: 0,
    cartTax: 0,
    cartTotal: 0,
    storeProducts: [],
    filteredProducts: [],
    featuredProducts: [],
    singleProduct: {},
    loading: false,
    max: 0,
    min: 0,
    search: "",
    company: "all",
    shipping: false,
    price: 0,
  };
  componentDidMount() {
    // Set Products Json Data
    this.setProducts(items);
    // Set Contentful
    // client
    //   .getEntries({ content_type: "storeProducts" })
    //   .then((response) => this.setProducts(response.items))
    //   .catch(console.error);
  }
  //Set Products
  setProducts = (products) => {
    let storeProducts = products.map((item) => {
      const { id } = item.sys;
      const image = item.fields.image.fields.file.url;
      const product = { id, ...item.fields, image };
      return product;
    });
    // Featured roducts
    let featuredProducts = storeProducts.filter(
      (item) => item.featured === true
    );
    // Max Price Products
    let maxPrice = Math.max(...storeProducts.map((item) => item.price));

    this.setState(
      {
        storeProducts,
        filteredProducts: storeProducts,
        featuredProducts,
        cart: this.getStorageCart(),
        singleProduct: this.getStorageProduct(),
        loading: false,
        price: maxPrice,
        max: maxPrice,
      },
      () => {
        this.addTotals();
      }
    );
  };
  // Get Cart from Local Storage
  getStorageCart = () => {
    let cart;
    if (localStorage.getItem("cart")) {
      cart = JSON.parse(localStorage.getItem("cart"));
    } else {
      cart = [];
    }
    return cart;
  };
  // Get Products from Local Storage
  getStorageProduct = () => {
    return localStorage.getItem("singleProduct")
      ? JSON.parse(localStorage.getItem("singleProduct"))
      : {};
  };
  // Get Totals
  getTotals = () => {
    let subTotal = 0;
    let cartItems = 0;
    this.state.cart.forEach((item) => {
      subTotal += item.total;
      cartItems += item.count;
    });
    subTotal = parseFloat(subTotal.toFixed(2));
    let tax = subTotal * 0.2;
    tax = parseFloat(tax.toFixed(2));
    let total = subTotal + tax;
    total = parseFloat(total.toFixed(2));
    return {
      cartItems,
      subTotal,
      tax,
      total,
    };
  };
  // Add Totals
  addTotals = () => {
    const totals = this.getTotals();
    this.setState({
      cartItems: totals.cartItems,
      cartTax: totals.tax,
      cartTotal: totals.total,
      cartSubTotal: totals.subTotal,
    });
  };
  // Sync Storage
  syncStorage = () => {
    localStorage.setItem("cart", JSON.stringify(this.state.cart));
  };
  // Add To Cart
  addToCart = (id) => {
    let tempCart = [...this.state.cart];
    let tempProducts = [...this.state.storeProducts];
    let tempItem = tempCart.find((item) => item.id === id);
    if (!tempItem) {
      let tempItem = tempProducts.find((item) => item.id === id);
      let total = tempItem.price;
      let cartItem = { ...tempItem, count: 1, total };
      tempCart = [...tempCart, cartItem];
    } else {
      tempItem.count++;
      tempItem.total = tempItem.price * tempItem.count;
      tempItem.total = parseFloat(tempItem.total.toFixed(2));
    }
    this.setState(
      () => {
        return { cart: tempCart };
      },
      () => {
        this.addTotals();
        this.syncStorage();
        this.openCart();
      }
    );
  };
  // Set Single Product
  setSingleProduct = (id) => {
    let product = this.state.storeProducts.find((item) => item.id === id);
    localStorage.setItem("singleProduct", JSON.stringify(product));
    this.setState({
      singleProduct: { ...product },
    });
    console.log("Set Single Product: ", id);
  };
  // Handle Side Bar
  handleSidebar = () => {
    this.setState({
      sidebarOpen: !this.state.sidebarOpen,
      loading: false,
    });
  };
  // Handle Cart
  handleCart = () => {
    this.setState({
      cartOpen: !this.state.cartOpen,
    });
  };
  // Close Cart
  closeCart = () => {
    this.setState({
      cartOpen: false,
    });
  };
  // Open Cart
  openCart = () => {
    this.setState({
      cartOpen: true,
    });
  };
  // Handle Filtering
  handleChange = (event) => {
    let name = event.target.name;
    let value =
      event.target.type === "checkbox"
        ? event.target.checked
        : event.target.value;
    this.setState(
      {
        [name]: value,
      },
      this.sortData
    );
  };
  // Sort Data
  sortData = () => {
    const { storeProducts, shipping, price, search, company } = this.state;
    let tempProducts = [...storeProducts];
    // Sort by Price
    let tempPrice = parseInt(price);
    tempProducts = tempProducts.filter((item) => item.price <= tempPrice);

    // Sort by Company
    if (company !== "all") {
      tempProducts = tempProducts.filter((item) => item.company === company);
    }
    // Sort by Free Shipping
    if (shipping) {
      tempProducts = tempProducts.filter((item) => item.freeShipping === true);
    }
    // Sort by Search
    if (search.length > 0) {
      tempProducts = tempProducts.filter((item) => {
        let tempSearch = search.toLowerCase();
        let tempTitle = item.title.toLowerCase().slice(0, search.length);
        if (tempSearch === tempTitle) {
          return item;
        }
      });
    }
    this.setState({ filteredProducts: tempProducts });
  };
  
  // CART FUNCTIONALITY //
  /**********************/
  // Increment Item Cart
  incrementItemCart = (id) => {
    let tempCart = [...this.state.cart];
    let cartItem = tempCart.find((item) => item.id === id);
    cartItem.count++;
    cartItem.total = parseFloat((cartItem.price * cartItem.count).toFixed(2));
    this.setState(
      () => {
        return { cart: [...tempCart] };
      },
      () => {
        this.addTotals();
        this.syncStorage();
      }
    );
  };
  // Decrement Item Cart
  decrementItemCart = (id) => {
    let tempCart = [...this.state.cart];
    let cartItem = tempCart.find((item) => item.id === id);
    cartItem.count = cartItem.count - 1;
    if (cartItem.count === 0) {
      this.removeItemCart(id);
    } else {
      cartItem.total = parseFloat((cartItem.price * cartItem.count).toFixed(2));
      this.setState(
        () => {
          return { cart: [...tempCart] };
        },
        () => {
          this.addTotals();
          this.syncStorage();
        }
      );
    }
  };
  // Remove Item Cart
  removeItemCart = (id) => {
    let tempCart = [...this.state.cart];
    tempCart = tempCart.filter((item) => item.id !== id);
    this.setState(
      () => {
        return { cart: [...tempCart] };
      },
      () => {
        this.addTotals();
        this.syncStorage();
      }
    );
  };
  // Clear All items into Cart
  clearAllItemsCart = () => {
    this.setState(
      () => {
        return { cart: [] };
      },
      () => {
        this.addTotals();
        this.syncStorage();
      }
    );
  };
  render() {
    return (
      <ProductContext.Provider
        value={{
          ...this.state,
          handleSidebar: this.handleSidebar,
          handleCart: this.handleCart,
          closeCart: this.closeCart,
          openCart: this.openCart,
          addToCart: this.addToCart,
          setSingleProduct: this.setSingleProduct,
          incrementItemCart: this.incrementItemCart,
          decrementItemCart: this.decrementItemCart,
          removeItemCart: this.removeItemCart,
          clearAllItemsCart: this.clearAllItemsCart,
          handleChange: this.handleChange,
        }}
      >
        {this.props.children}
      </ProductContext.Provider>
    );
  }
}

const ProductConsumer = ProductContext.Consumer;

export { ProductProvider, ProductConsumer };
